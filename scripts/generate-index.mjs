import fs from 'fs';
import path from 'path';
// import Fuse from 'fuse.js';
import { getAllPostDataIndex } from '../lib/posts.mjs';
import { cwd } from '../lib/utils.mjs';
import JSZip from 'jszip';

export default async function buildIndex() {
  console.log(`Building post index...`);

  const indexData = (await getAllPostDataIndex()) ?? [];

  const outputFolder = path.join(cwd(), 'public');

  // Write JSON index file
  fs.writeFileSync(path.join(outputFolder, 'index.json'), JSON.stringify(indexData));

  // Compress JSON index file
  var zip = new JSZip();

  zip.file('index.json', JSON.stringify(indexData));

  if (JSZip.support.nodebuffer) {
    await zip.generateAsync({
      type: 'nodebuffer',
      compression: 'DEFLATE'
    })
      .then((content) => {
        fs.writeFileSync(path.join(outputFolder, 'index.zip'), content);
      });
  } else if (JSZip.support.uint8array) {
    await zip.generateAsync({
      type: 'uint8array',
      compression: 'DEFLATE'
    })
      .then((content) => {
        fs.writeFileSync(path.join(outputFolder, 'index.zip'), content);
      });
  } else {
    await zip.generateAsync({
      type: 'base64',
      compression: 'DEFLATE'
    })
      .then((content) => {
        fs.writeFileSync(path.join(outputFolder, 'index.zip'), content);
      });
  }

  console.log('Done!');
}
