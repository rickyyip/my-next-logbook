import("./generate-index.mjs").then(async ({ default: buildIndex }) => {
  await buildIndex();
});
