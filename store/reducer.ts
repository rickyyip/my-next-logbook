import { combineReducers } from 'redux';

import searchModalReducer from './slices/searchModal';

const rootReducer = combineReducers({
  searchModal: searchModalReducer,
});

export default rootReducer;
