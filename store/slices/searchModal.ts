import { createSlice } from '@reduxjs/toolkit';

export const searchModalSlice = createSlice({
  name: 'searchModal',
  initialState: {
    isOpen: false,
  },
  reducers: {
    toggleSearchModal: (state) => {
      state.isOpen = !state.isOpen;
    },
    openSearchModal: (state) => {
      state.isOpen = true;
    },
    closeSearchModal: (state) => {
      state.isOpen = false;
    }
  },
});

// Action creators are generated for each case reducer function
export const { toggleSearchModal, openSearchModal, closeSearchModal } = searchModalSlice.actions;

export default searchModalSlice.reducer;
