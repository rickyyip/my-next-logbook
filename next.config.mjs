/** @type {import('next').NextConfig} */

import remarkFrontmatter from 'remark-frontmatter';
import mdx from '@next/mdx';
import rehypeHighlight from 'rehype-highlight';
import remarkGfm from 'remark-gfm';

const withMDX = mdx({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [remarkFrontmatter, remarkGfm],
    rehypePlugins: [rehypeHighlight],
    // providerImportSource: "@mdx-js/react",
  },
})

export default withMDX({
  pageExtensions: ['ts', 'tsx', 'js', 'jsx', 'md', 'mdx'],
  reactStrictMode: true,
  webpack: (config, { dev, isServer }) => {
    return config;
  }
})
