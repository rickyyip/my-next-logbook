import Image from 'next/image';
import {
  faCloudflare,
  faCss3Alt,
  faDigitalOcean,
  faDocker,
  faGitAlt,
  faHtml5,
  faJava,
  faJenkins,
  faNodeJs,
  faPython,
  faReact,
  faVuejs,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import profilePic from '../public/images/profile/profile-pic-2.jpg';
import cLangIcon from '../public/images/icons/c-lang.png';
import rLangIcon from '../public/images/icons/r-lang.png';
import sailsJs from '../public/images/icons/sailsjs-logo.png';
import ThreeBackground from '../components/three-background';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faQuestion } from '@fortawesome/free-solid-svg-icons';
import React, { FunctionComponent, ReactElement, ReactNode } from 'react';
import Layout from '../components/layout';
import { useTheme } from 'next-themes';

const About: FunctionComponent & {
  getLayout: (props: ReactElement) => JSX.Element;
} = () => {
  const Skills = (props: { children: ReactNode }) => {
    return (
      <div className="my-4 flex flex-wrap items-center justify-center gap-8">
        {props.children}
      </div>
    );
  };

  const SkillIcon = (props: { icon?: IconProp; children?: ReactNode }) => {
    return (
      <div className="w-12 drop-shadow-md">
        {props?.icon && <FontAwesomeIcon icon={props.icon ?? faQuestion} />}
        {props.children && props.children}
      </div>
    );
  };

  return (
    <div className="mb-16 text-center">
      <div className="my-8 overflow-visible">
        <Image
          className="rounded-full"
          src={profilePic}
          alt="Ricky Yip"
          width="200"
          height="200"
          placeholder="blur"
        />
      </div>

      <h1 id="my-name" className="text-3xl dark:text-gray-50">Ricky Yip</h1>

      <h2 id="my-location" className="text-2xl text-blue-500">Sydney, Australia</h2>

      <h2 id="my-role" className="mt-2 text-center text-lg text-blue-500">
        Full-Stack Developer
      </h2>

      <hr className="mx-auto my-10 w-1/2 border-gray-900 border-opacity-50 dark:border-gray-500" />

      <Skills>
        <SkillIcon icon={faNodeJs} />
        <SkillIcon icon={faHtml5} />
        <SkillIcon icon={faCss3Alt} />
        <SkillIcon icon={faJava} />
        <SkillIcon icon={faPython} />
        <SkillIcon>
          <Image
            className="brightness-0 filter dark:invert"
            src={cLangIcon}
            alt="C Lang"
          />
        </SkillIcon>
        <SkillIcon>
          <Image
            className="brightness-0 filter dark:invert"
            src={rLangIcon}
            alt="R Lang"
          />
        </SkillIcon>
      </Skills>

      <Skills>
        <SkillIcon icon={faReact} />
        <SkillIcon icon={faVuejs} />
        <SkillIcon>
          <Image
            className="brightness-0 filter dark:invert"
            src={sailsJs}
            alt="Sails.js"
          />
        </SkillIcon>
      </Skills>

      <Skills>
        <SkillIcon icon={faGitAlt} />
        <SkillIcon icon={faJenkins} />
        <SkillIcon icon={faDocker} />
        <SkillIcon icon={faCloudflare} />
        <SkillIcon icon={faDigitalOcean} />
      </Skills>

      {/* <div className="flex justify-center my-8">
        <button @click="modal = 'kofi'">
            <div className="py-2 px-4 rounded-xl bg-purple-600 flex items-center">
                <Image className="h-4 mr-4" src="/img/Kofi_pixel_logo.png" />
                Buy Me a Coffee
            </div>
        </button>
    </div> */}

      <div className="fixed top-0 left-0 -z-10">
        <ThreeBackground />
      </div>
    </div>
  );
};

export default About;

About.getLayout = function getLayout(page: ReactElement) {
  return <Layout bgTransparent={true} title="About">{page}</Layout>;
};
