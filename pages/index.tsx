import Layout from '@/components/layout';
import PostListItem from '@/components/post-list-item';
import { GetStaticProps } from 'next';
import { FunctionComponent, ReactElement } from 'react';
import Paginator from '../components/paginator';
import { getLatestPosts, getPageCount, getPostCount } from '../lib/posts.mjs';

type PostData = {
  id: string;
  fullPath: string;
  title: string;
  date: string;
  contentHtml: string;
  excerptHtml: string;
  isShort: boolean;
};

type Props = {
  latestPosts: Array<any>;
  totalPages: number;
  totalPosts: number;
};

const Home: FunctionComponent<Props> & {
  getLayout: (props: ReactElement) => JSX.Element;
} = ({ latestPosts, totalPages, totalPosts }: Props) => {
  return (
    <>
      <div className="flex items-baseline justify-between">
        <h1 className="text-3xl">Latest Posts</h1>
        <p
          id="post-count"
          className="rounded-full bg-gray-100 py-1 px-3 dark:bg-gray-500"
        >
          {totalPosts} Posts
        </p>
      </div>

      <section className="postList mt-12">
        <ul>
          {latestPosts &&
            latestPosts.map(({ id, title, date, excerptHtml }: PostData) => (
              <PostListItem
                key={id}
                id={id}
                title={title}
                date={date}
                excerptHtml={excerptHtml}
              />
            ))}
        </ul>
      </section>

      <Paginator
        urlBase="/page/"
        currentPage={1}
        lastPage={totalPages}
      ></Paginator>
    </>
  );
};

export default Home;

export const getStaticProps: GetStaticProps = async () => {
  const postsPerPage = 10;

  let latestPosts = await getLatestPosts({ skip: 0, limit: postsPerPage });

  const totalPages = await getPageCount({ countPerPage: postsPerPage });
  const totalPosts = await getPostCount();

  return {
    props: {
      latestPosts,
      totalPages,
      totalPosts,
    },
  };
};

Home.getLayout = function getLayout(page: ReactElement) {
  return <Layout bgTransparent={true}>{page}</Layout>;
};
