import '../styles/globals.scss';
import '../styles/fonts.css';

import type { AppProps } from 'next/app';
import type { NextComponentType } from 'next';
import { ThemeProvider } from 'next-themes';
import Layout from '../components/layout';
import store from '../store/store';
import { Provider } from 'react-redux';
import KeyEventHandler from '../components/key-event-handler';

import { ReactElement } from 'react';

type NextComponentTypeThemed = NextComponentType & {
  theme?: string;
  getLayout?: (page: ReactElement) => any;
};

interface AppPropsThemed extends AppProps {
  Component: NextComponentTypeThemed;
}

function ReactApp({ Component, pageProps }: AppPropsThemed) {
  // Use the layout defined at the page level, if available
  const getLayout =
    Component.getLayout || ((page: ReactElement) => <Layout>{page}</Layout>);

  return (
    <ThemeProvider forcedTheme={Component.theme || undefined} attribute="class">
      <Provider store={store}>
        {getLayout(
          <KeyEventHandler>
            <Component {...pageProps} />
          </KeyEventHandler>
        )}
      </Provider>
    </ThemeProvider>
  );
}

export default ReactApp;
