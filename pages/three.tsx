import { FunctionComponent, ReactElement } from 'react';
import ThreeBackground from '../components/three-background';

const Three: FunctionComponent & {
  getLayout: (props: ReactElement) => JSX.Element;
} = () => {
  return <ThreeBackground />;
};

export default Three;

Three.getLayout = function getLayout(page: ReactElement) {
  return <div>{page}</div>;
};
