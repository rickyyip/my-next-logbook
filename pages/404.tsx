export default function ErrorNotFound() {
  return (
    <div className="flex flex-col items-center">
      <h1 className="text-4xl">404</h1>
      <h1>Sorry, the page could not be found.</h1>
    </div>
  );
}
