import sys

class HelloClass():
    def __init__(self, name):
        if name == '':
            print('Warning: empty string!', file = sys.stderr)

        self.name = name

    def hello(self):
        print('Hi, {}'.format(self.name))

    def update_name(self):
        new_name = input('Your new name: ')
        self.name = new_name
        print('You have changed your name!')

def main():
    print('main() is called')

if __name__ == '__main__':
    main()
