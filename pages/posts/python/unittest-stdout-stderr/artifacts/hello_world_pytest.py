from io import StringIO

# Import our program
import hello_world as hw

def test_main(capsys):

    # Test main()
    hw.main()
    expected_out = 'main() is called\n'

    # Read captured outputs
    captured = capsys.readouterr()

    # Check stdout
    assert captured.out == expected_out


def test_constructor(capsys):

    # Test empty string passed to HelloClass contructor
    hc = hw.HelloClass('')
    hc.hello()

    # Expect a warning message to stderr
    expected_err = 'Warning: empty string!\n'
    
    # Expect an empty string used as name
    expected_out = 'Hi, \n'

    # Read captured outputs
    captured = capsys.readouterr()

    # Check stdout and stderr
    assert captured.out == expected_out
    assert captured.err == expected_err


def test_change_name(capsys, monkeypatch):

    monkeypatch.setattr('sys.stdin', StringIO('Bob\n'))

    # Initial class
    hc = hw.HelloClass('John')

    hc.hello()  # Hi, John
    hc.update_name()    # stdin is read
    hc.hello()  # Hi, Bob

    expected_out = """Hi, John
Your new name: You have changed your name!
Hi, Bob
"""
    # Read captured outputs
    captured = capsys.readouterr()

    # Check stdout and stderr
    assert captured.out == expected_out

