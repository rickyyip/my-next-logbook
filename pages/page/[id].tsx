import Layout from '@/components/layout';
import PostListItem from '@/components/post-list-item';
import { FunctionComponent, ReactElement } from 'react';
import Paginator from '../../components/paginator';
import { getLatestPosts, getPageCount } from '../../lib/posts.mjs';

type PostData = {
  id: string;
  fullPath: string;
  title: string;
  date: string;
  contentHtml: string;
  excerptHtml: string;
  isShort: boolean;
};

type Props = {
  latestPosts: Array<any>;
  totalPages: number;
  pageNumber: number;
};

const Page: FunctionComponent<Props> & {
  getLayout: (props: ReactElement) => JSX.Element;
} = ({ latestPosts, totalPages, pageNumber }: Props) => {
  return (
    <>
      <div className="flex items-baseline justify-between">
        <h1 className="text-3xl">Latest Posts: P.{pageNumber}</h1>
      </div>

      <section className="postList mt-6">
        <ul>
          {latestPosts &&
            latestPosts.map(
              ({ id, fullPath, title, date, excerptHtml }: PostData) => (
                <PostListItem
                  key={id}
                  id={id}
                  title={title}
                  date={date}
                  excerptHtml={excerptHtml}
                />
              )
            )}
        </ul>
      </section>

      <Paginator
        urlBase="/page/"
        currentPage={pageNumber}
        lastPage={totalPages}
      ></Paginator>
    </>
  );
};

const postsPerPage = 10;

export async function getStaticProps({ params }: any) {
  const pageNumber: number = params.id ?? 1;

  let latestPosts = await getLatestPosts({
    skip: (pageNumber - 1) * postsPerPage,
    limit: postsPerPage,
  });

  const totalPages = await getPageCount({ countPerPage: postsPerPage });

  return {
    props: {
      latestPosts,
      totalPages,
      pageNumber,
    },
  };
}

export default Page;

export async function getStaticPaths() {
  const totalPages = (await getPageCount({ countPerPage: postsPerPage })) ?? 0;

  const paths: { params: { id: string } }[] = [];

  for (let i = 1; i <= totalPages; i++) {
    paths.push({ params: { id: i.toString() } });
  }

  return {
    paths,
    fallback: false,
  };
}

Page.getLayout = function getLayout(page: ReactElement) {
  return <Layout bgTransparent={true}>{page}</Layout>;
};
