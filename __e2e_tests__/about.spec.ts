import { test, expect } from '@playwright/test';

test('should contain correct about info', async ({ page }) => {
  // Start from the index page (the baseURL is set via the webServer in the playwright.config.ts)
  await page.goto('/about');
  await expect(page).toHaveURL('/about');
  await expect(page.locator('id=my-name')).toContainText('Ricky Yip');
  await expect(page.locator('id=my-location')).toContainText('Sydney, Australia');
  await expect(page.locator('id=my-role')).toContainText('Full-Stack Developer');
});
