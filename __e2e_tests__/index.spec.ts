import { test, expect } from '@playwright/test';

test('should navigate to the about page', async ({ page, isMobile }) => {
  // Start from the index page (the baseURL is set via the webServer in the playwright.config.ts)
  await page.goto('/');

  // Toggle mobile menu
  if (isMobile) {
    await expect(page.locator('id=topbar-menu')).toBeHidden();
    await page.click('id=mobile-menu-toggle');
  }

  await expect(page.locator('id=topbar-menu')).toBeVisible();
  await expect(page.locator('id=menu-about-link')).toBeVisible();
  await page.click('id=menu-about-link');
  await expect(page).toHaveURL('/about');
});
