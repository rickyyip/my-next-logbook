module.exports = {
  content: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx,css,scss}", "./styles/*.{css,scss}"],
  darkMode: 'class',
  theme: {
    extend: {
      minHeight: {
        '0': '0',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        '80vh': '80vh',
        '90vh': '90vh',
        'full': '100%',
      },

      maxHeight: {
        '80vh': '80vh',
        '85vh': '85vh',
        '90vh': '90vh',
      },

      minWidth: {
        '2xs': '16rem',
      },

      maxWidth: {
        'screen': '100vw',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
    },
  },
  plugins: [],
}
