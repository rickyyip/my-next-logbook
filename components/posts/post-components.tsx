import Image from 'next/image';
import { FunctionComponent, ReactNode } from 'react';

type Props = { children: ReactNode };

const PostComponent: FunctionComponent<Props> = (props: Props) => {
  return <span className="post-component">{props.children}</span>;
};

export function Text({ children }: any) {
  return <p>{children}</p>;
}

export function Heading({ children }: any) {
  return <span>{children}</span>;
}

Heading.h1 = ({ children }: any) => {
  return <h1>{children}</h1>;
};
Heading.h2 = ({ children }: any) => {
  return <h2>{children}</h2>;
};
Heading.h3 = ({ children }: any) => {
  return <h3>{children}</h3>;
};

export function Img(props: any) {
  return (
    <Image
      {...props}
      alt={props.alt ?? ''}
      layout="responsive"
      loading="lazy"
    />
  );
}
