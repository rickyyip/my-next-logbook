// Dependencies
import { FunctionComponent, MouseEvent, ReactNode, useState } from 'react';
import classNames from 'classnames';
import dynamic from 'next/dynamic';

// Components
import Link from 'next/link';
import Image from 'next/image';

// Dynamic Components
const SearchModalToggle = dynamic(
  () => import('./buttons/search-modal-toggle')
);
const DarkModeToggle = dynamic(() => import('./buttons/dark-mode-toggle'), {
  ssr: false,
});

// Assets
import logoImage from '../public/images/logo/RY_logo_ring_500.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';

type Props = { menuOpen?: boolean };

const Topbar: FunctionComponent<Props> = ({ menuOpen }: Props) => {
  const [expand, setExpand] = useState(menuOpen ?? false);

  function toggleMenu(e: MouseEvent) {
    setExpand(!expand);
  }

  function closeMenu() {
    setExpand(false);
  }

  type LinkProps = {
    children: ReactNode;
    href: string;
  };

  const MenuLink: FunctionComponent<LinkProps> = ({
    children,
    href,
  }: LinkProps) => {
    return (
      <span onClick={closeMenu}>
        <Link href={href}>{children}</Link>
      </span>
    );
  };

  return (
    <nav className="relative z-10 mx-auto flex h-auto w-full flex-col bg-gray-100 p-3 text-gray-900 dark:bg-gray-700 dark:text-gray-50 md:w-5/6 md:flex-row md:items-center md:rounded-lg md:px-4 md:py-2 xl:w-4/6">
      <div className="flex flex-grow items-center">
        <div id="branding" className="flex items-center">
          <MenuLink href="/">
            <a className="flex items-center">
              <Image src={logoImage} alt="RY" height={40} width={40} />
            </a>
          </MenuLink>
          <Link href="/">
            <a className="ml-3 text-xl font-extrabold dark:font-bold">
              Logbook
            </a>
          </Link>
        </div>
        <button
          id="mobile-menu-toggle"
          className={classNames({
            'ml-auto h-7 w-7 text-2xl transition md:hidden': true,
            'rotate-90': expand,
          })}
          onClick={toggleMenu}
        >
          <FontAwesomeIcon icon={faEllipsisH} />
        </button>
      </div>

      <div
        id="topbar-menu"
        className={classNames({
          'mt-4 ml-2 inline-block transition-all md:m-0 md:flex md:items-center':
            true,
          hidden: !expand,
        })}
      >
        <div className="p-1 md:mr-4">
          <MenuLink href="/">
            <a className="text-lg text-gray-800 hover:text-blue-600 dark:text-gray-50">
              Home
            </a>
          </MenuLink>
        </div>
        <div className="p-1 md:mr-4">
          <MenuLink href="/about">
            <a id="menu-about-link" className="text-lg text-gray-800 hover:text-blue-600 dark:text-gray-50">
              About
            </a>
          </MenuLink>
        </div>
        <div className="mt-2 md:m-0">
          <SearchModalToggle onClick={closeMenu} />
        </div>
        <div className="mt-2 md:m-0 md:ml-2">
          <DarkModeToggle />
        </div>
      </div>
    </nav>
  );
};

export default Topbar;
