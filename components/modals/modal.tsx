import React, {
  FunctionComponent,
  KeyboardEventHandler,
  MouseEventHandler,
  ReactNode,
} from 'react';
import classNames from 'classnames';

import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type Props = {
  children: ReactNode;
  isOpen: boolean;
  title: string;
  onClose: MouseEventHandler;
  onKeyUp?: KeyboardEventHandler;
};

const Modal: FunctionComponent<Props> = ({
  children,
  isOpen,
  title,
  onClose,
  onKeyUp,
}: Props) => {
  return (
    <div
      className={classNames({
        'fixed top-0 left-0 z-30 h-screen w-screen overflow-hidden bg-gray-300 bg-opacity-70 dark:bg-gray-600 dark:bg-opacity-30': 1,
        hidden: !isOpen,
      })}
      tabIndex={-1}
      onKeyUp={onKeyUp}
    >
      <div className="h-full w-full md:my-4 md:p-8">
        <div className="mx-auto flex h-full w-full flex-col bg-gray-400 bg-opacity-90 p-4 dark:bg-gray-700 md:w-5/6 md:p-4 xl:w-4/6 rounded-2xl">
          <div className="mt-4 mb-4 mr-4 flex items-start justify-between text-gray-800 dark:text-gray-100">
            <h1 className="ml-4">{title}</h1>
            <button
              className="flex h-9 w-9 items-center justify-center rounded-md border-[1px] border-gray-800 p-2 text-gray-800 dark:border-gray-400 dark:text-gray-200"
              onClick={onClose}
            >
              {/* <FontAwesomeIcon icon={faTimes} /> */}
              <span className="font-sans text-xl font-extrabold">X</span>
            </button>
          </div>

          <div className="h-full overflow-auto p-4">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
