import React, {
  FunctionComponent,
  MouseEvent,
  MouseEventHandler,
  ReactNode,
  useEffect,
  useRef,
  useState,
} from 'react';

import Fuse from 'fuse.js';
var MarkdownIt = require('markdown-it');

import { useAppSelector, useAppDispatch } from '../../store/hooks';
import { closeSearchModal } from '../../store/slices/searchModal';

import styles from './search-modal.module.scss';
import Link from 'next/link';
import Modal from './modal';

type LinkProps = {
  children: ReactNode;
  href: string;
  onClick: MouseEventHandler;
};

const ModalLink: FunctionComponent<LinkProps> = ({
  children,
  href,
  onClick,
}: LinkProps) => {
  return (
    <span onClick={onClick}>
      <Link href={href}>{children}</Link>
    </span>
  );
};

const SearchModal: FunctionComponent = () => {
  const isOpen = useAppSelector((state) => state.searchModal.isOpen);
  const dispatch = useAppDispatch();

  async function handleClose() {
    dispatch(closeSearchModal());
  }

  async function handleKeyUp(e: any) {
    if (e?.code === 'Escape') {
      console.log(`Modal: ESCAPE`);
      handleClose();
    }
  }

  type indexEntryType = {
    id: string;
    title: string;
    date: string;
    content: string;
  };

  const fuseOptions = {
    isCaseSensitive: false,
    // includeScore: false,
    shouldSort: true,
    // includeMatches: false,
    // findAllMatches: false,
    minMatchCharLength: 3,
    threshold: 0.6,
    location: 0,
    distance: 200,
    // useExtendedSearch: false,
    // ignoreLocation: true,
    // ignoreFieldNorm: false,
    // fieldNormWeight: 1,
    keys: ['id', 'title', 'content', 'date'],
  };

  const [searchReady, setSearchReady] = useState(false);
  const [searchError, setSearchError] = useState(false);
  const [indexData, setIndexData] = useState(new Array<indexEntryType>());

  let fuse: Fuse<indexEntryType> = new Fuse(indexData, fuseOptions);

  type resultType = {
    index: number;
    id: string;
    title: string;
    date: string;
    content: string;
  };

  const [searchTerms, setSearchTerms] = useState('');
  const [searchResults, setSearchResults] = useState(new Array<resultType>());

  const searchInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (!isOpen) {
      return;
    }
    if (searchReady) {
      searchInputRef.current?.focus();
      return;
    }
    fetch('/index.json')
      .then((res) => {
        let data = res.json();
        return data;
      })
      .then((data) => {
        setIndexData(data);
        setSearchError(false);
        setSearchReady(true);
      })
      .catch((err) => {
        console.log(`Error fetching search index: ${err?.message}`);
        setSearchError(true);
      });
  }, [isOpen, searchReady]);

  async function handleSearchTerms(e: any) {
    if (!searchReady || fuse === undefined) {
      return;
    }
    setSearchTerms(e.target.value);

    let rawResults = await fuse.search(searchTerms);
    const results = rawResults.map((r) => {
      return {
        index: r.refIndex,
        ...r.item,
      };
    });
    setSearchResults(results);
  }

  const md = new MarkdownIt();

  return (
    <Modal
      title="Search"
      isOpen={isOpen}
      onClose={handleClose}
      onKeyUp={handleKeyUp}
    >
      {searchError && (
        <div className="flex justify-center">
          <h1 className="my-6">Error: Problem loading index data.</h1>
        </div>
      )}
      {!searchReady && (
        <div className="flex justify-center">
          <h1>Loading....</h1>
        </div>
      )}
      {searchReady && (
        <div className="flex flex-col">
          <div className="flex flex-wrap items-center gap-4 rounded-lg bg-gray-300 py-2 px-4 text-xl dark:bg-gray-500">
            <input
              className="flex-grow bg-transparent p-1 focus:ring-0"
              type="text"
              onChange={handleSearchTerms}
              value={searchTerms}
              autoFocus={true}
              ref={searchInputRef}
            ></input>

            {searchResults.length > 0 && (
              <span className="rounded-full bg-gray-600 py-1 px-2 text-sm text-gray-50">
                {searchResults.length} Results
              </span>
            )}
          </div>

          <div className="m-4 flex-grow">
            {searchResults.map((post) => {
              const parseContent = md.render(post.content);

              return (
                <div key={post.index}>
                  <div className={styles.searchResultItem}>
                    <span>
                      <ModalLink href={post.id} onClick={handleClose}>
                        <a>
                          <h1>{post.title}</h1>
                        </a>
                      </ModalLink>
                    </span>

                    <p>{post.date.replace(/[TZ].*$/, '')}</p>
                    <p
                      className={styles.searchResultItemPreview}
                      dangerouslySetInnerHTML={{ __html: parseContent }}
                    ></p>
                  </div>

                  <div className="flex justify-center">
                    <ModalLink href={post.id} onClick={handleClose}>
                      <a className="hover:text-yellow-600 dark:hover:text-yellow-200">
                        Read More
                      </a>
                    </ModalLink>
                  </div>

                  <hr className="my-6" />
                </div>
              );
            })}
          </div>
        </div>
      )}
    </Modal>
  );
};

export default SearchModal;
