import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FunctionComponent, MouseEvent } from 'react';

import { useAppDispatch } from '../../store/hooks';
import { openSearchModal } from '../../store/slices/searchModal';

type Props = {
  onClick?: (e: MouseEvent) => any;
};

const SearchModalToggle: FunctionComponent<Props> = ({ onClick }: Props) => {
  const dispatch = useAppDispatch();

  async function handleClick(e: MouseEvent) {
    dispatch(openSearchModal());
    onClick !== undefined ? onClick(e) : null;
  }

  return (
    <>
      <button
        className="flex h-10 w-full items-center justify-center rounded-lg bg-gray-200 p-2 dark:bg-gray-500 md:w-10"
        onClick={handleClick}
      >
        <span className="w-5">
          <FontAwesomeIcon icon={faSearch} />
        </span>
      </button>
    </>
  );
};

export default SearchModalToggle;
