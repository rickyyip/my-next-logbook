import { useTheme } from 'next-themes';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSun, faMoon, faPalette } from '@fortawesome/free-solid-svg-icons';
import { FunctionComponent } from 'react';

const DarkModeToggle: FunctionComponent = () => {
  const { theme, setTheme } = useTheme();

  function toggleTheme() {
    if (theme === 'system') {
      setTheme('light');
    } else if (theme === 'light') {
      setTheme('dark');
    } else {
      setTheme('system');
    }
  }

  return (
    <button
      className="flex h-10 w-full items-center justify-center rounded-lg bg-gray-200 p-2 dark:bg-gray-500 md:w-10"
      onClick={toggleTheme}
    >
      <span className="w-5">
        {(!theme || theme === 'system') && <FontAwesomeIcon icon={faPalette} />}
        {theme === 'light' && <FontAwesomeIcon icon={faSun} />}
        {theme === 'dark' && <FontAwesomeIcon icon={faMoon} />}
      </span>

      <span className="ml-2 font-bold capitalize md:hidden">
        {!theme || theme === 'system' ? 'Auto' : theme}
      </span>
    </button>
  );
};

export default DarkModeToggle;
