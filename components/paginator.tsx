import Link from 'next/link';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { FunctionComponent } from 'react';

type Props = {
  currentPage: number;
  lastPage: number;
  urlBase: string;
};

const Paginator: FunctionComponent<Props> = (props: Props) => {
  const { currentPage, lastPage, urlBase } = props;

  const pageNumber: string = currentPage.toString() ?? '1';

  const paginatorList: Array<any> = [];

  if (lastPage > 2) {
    let paddingBefore = false;
    let paddingAfter = false;
    for (let p = 2; p < lastPage; p++) {
      if (currentPage - p > 3 || (currentPage - p > 2 && p !== 2)) {
        if (!paddingBefore) {
          paddingBefore = true;
          paginatorList.push(
            <a>
              <span className="paginator-link">...</span>
            </a>
          );
        }
        continue;
      } else if (
        p - currentPage > 3 ||
        (p - currentPage > 2 && p !== lastPage - 1)
      ) {
        if (!paddingAfter) {
          paddingAfter = true;
          paginatorList.push(
            <a>
              <span className="paginator-link">...</span>
            </a>
          );
        }
        continue;
      }
      paginatorList.push(
        <Link
          href={parseInt(pageNumber) !== p ? urlBase + p : '#'}
          key={'page-' + p}
        >
          <a className={parseInt(pageNumber) === p ? 'link-active' : undefined}>
            <span className="paginator-link">{p}</span>
          </a>
        </Link>
      );
    }
  }

  return (
    <>
      <div className="paginator">
        <Link
          href={currentPage > 1 ? urlBase + (parseInt(pageNumber) - 1) : '#'}
        >
          <a className={currentPage > 1 ? undefined : 'link-disabled'}>
            <span className="paginator-btn rounded-l-lg">
              <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
            </span>
          </a>
        </Link>
        <Link href={urlBase + '1'} key={'page-' + 1}>
          <a className={currentPage <= 1 ? 'link-active' : undefined}>
            <span className="paginator-link">1</span>
          </a>
        </Link>
        {lastPage > 2 && paginatorList}
        {lastPage > 1 && (
          <Link href={urlBase + lastPage} key={'page-' + lastPage}>
            <a className={currentPage >= lastPage ? 'link-active' : undefined}>
              <span className="paginator-link">{lastPage}</span>
            </a>
          </Link>
        )}
        <Link
          href={
            currentPage < lastPage ? urlBase + (parseInt(pageNumber) + 1) : '#'
          }
        >
          <a className={currentPage < lastPage ? undefined : 'link-disabled'}>
            <span className="paginator-btn rounded-r-lg">
              <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
            </span>
          </a>
        </Link>
      </div>
    </>
  );
};

export default Paginator;
