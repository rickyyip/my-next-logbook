import React, {
  useRef,
  useMemo,
  useEffect,
  useState,
  FunctionComponent,
} from 'react';
import { Canvas, useFrame } from '@react-three/fiber';
import * as THREE from 'three';
import classNames from 'classnames';
import { useTheme } from 'next-themes';

const ThreeBackground: FunctionComponent = () => {
  const { theme, setTheme } = useTheme();

  function Dodecahedron(props: any) {
    // This reference gives us direct access to the THREE.Mesh object
    const ref: any = useRef();

    // Subscribe this component to the render-loop, rotate the mesh every frame
    useFrame((state, delta) => {
      ref.current.rotation.x += 0.0001;
      ref.current.rotation.y += 0.0002;
    });

    const edges = useMemo(() => new THREE.DodecahedronGeometry(), []);

    // Return the view, these are regular Threejs elements expressed in JSX
    return (
      <group dispose={null}>
        <lineSegments {...props} ref={ref} geometry={edges} renderOrder={100}>
          <lineBasicMaterial
            color={theme === 'dark' || theme === 'black' ? 'cyan' : 'black'}
            transparent={true}
            opacity={0.1}
          />
        </lineSegments>
      </group>
    );
  }

  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  return (
    <div className="fixed top-0 left-0 h-screen w-screen overflow-hidden">
      <Canvas
        className={classNames({
          'transition duration-1000': 1,
          'opacity-0': !loaded,
          'opacity-100': loaded,
        })}
      >
        <Dodecahedron position={[0, 0, 0]} scale={7} />
      </Canvas>
    </div>
  );
};

export default ThreeBackground;
