import Link from 'next/link';
import { FunctionComponent } from 'react';

type PostProps = {
  id: string;
  title: string;
  date?: string;
  excerptHtml: string;
};

const PostListItem: FunctionComponent<PostProps> = (props: PostProps) => {
  return (
    <li
      className="my-4 rounded-2xl bg-gray-200 p-2 dark:bg-gray-800 md:bg-gray-100 md:p-4 dark:md:bg-gray-700 xl:p-6"
      key={props.id}
    >
      <div className="w-full">
        <Link href={props.id}>
          <a>
            <h1
              id="post-title"
              className="font-semibold hover:text-blue-400 dark:hover:text-yellow-100"
            >
              {props.title}
            </h1>
          </a>
        </Link>
        <p id="post-date">
          {props.date ? props.date.replace(/[TZ].*$/, '') : 'No Date'}
        </p>
        <div
          className="contentWrapper postExcerpt"
          dangerouslySetInnerHTML={{ __html: props.excerptHtml }}
        />

        <div className="mt-4 flex justify-center">
          <Link href={props.id}>
            <a className="hover:text-yellow-600 dark:hover:text-yellow-200">
              Read More
            </a>
          </Link>
        </div>
      </div>

      <hr className="mx-auto my-8 w-3/4 border-gray-700 dark:border-gray-500 md:hidden" />
    </li>
  );
};

export default PostListItem;
