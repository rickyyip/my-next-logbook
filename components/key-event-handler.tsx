import { FunctionComponent, ReactElement, useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../store/hooks';
import { toggleSearchModal } from '../store/slices/searchModal';

type Props = {
  children: ReactElement;
};

const KeyEventHandler: FunctionComponent<Props> = ({ children }: Props) => {
  // const searchModalOpen = useAppSelector((state) => state.searchModal.isOpen);

  const dispatch = useAppDispatch();

  useEffect(() => {
    const handleKeyUp = async (e: any) => {
      if (e.altKey && e.code === 'KeyK') {
        dispatch(toggleSearchModal());
      }
    };

    document.addEventListener('keyup', handleKeyUp);
    console.log(`Registered keyboard event listener`);
  }, [dispatch]);

  return <>{children}</>;
};

export default KeyEventHandler;
