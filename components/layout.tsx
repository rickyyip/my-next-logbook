import { FunctionComponent, ReactNode } from 'react';
import Head from 'next/head';
import Topbar from './topbar';
import Favicon from './favicon';
import SearchModal from './modals/search-modal';
import classNames from 'classnames';

type Props = {
  children: ReactNode;
  title?: string;
  bgTransparent?: boolean;
};

const Layout: FunctionComponent<Props> = (props: Props) => {
  return (
    <div className="relative z-10 min-h-screen max-w-screen overflow-hidden bg-gray-200 text-gray-900 dark:bg-gray-800 dark:text-gray-50">
      <Head>
        <title>Logbook{props.title ? `: ${props.title}` : ''}</title>
      </Head>
      <Favicon />
      <div className="md:pt-4">
        <Topbar />
      </div>
      <div className="mx-auto w-11/12 py-4 md:w-5/6 md:py-6 xl:w-4/6">
        <main
          className={classNames({
            'rounded-xl md:p-4 lg:p-8': 1,
            'bg-gray-200 dark:bg-gray-800 md:bg-gray-100 dark:md:bg-gray-700 ':
              !props.bgTransparent,
          })}
        >
          {props.children}
        </main>
      </div>

      <SearchModal />
    </div>
  );
};

export default Layout;
