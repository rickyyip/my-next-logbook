import path from 'path';

/**
 * 
 * @returns {string} Project root folder path
 */
export function cwd() {
  return process.platform === 'win32' ? process.cwd().replaceAll('\\', '/') : process.cwd();
}

/**
 * 
 * @returns {string} Post data directory path
 */
export function postDir() {
  const dir = path.join(process.cwd(), 'pages', 'posts');
  return process.platform === 'win32' ? dir.replaceAll('\\', '/') : dir;
}

/**
 * 
 * @returns {string} Page data directory path
 */
export function pageDir() {
  const dir = path.join(process.cwd(), 'pages');
  return process.platform === 'win32' ? dir.replaceAll('\\', '/') : dir;
}
