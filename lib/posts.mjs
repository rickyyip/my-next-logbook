import fs from 'fs';
import path from 'path';
import glob from 'glob';
import matter from 'gray-matter';
import { unified } from 'unified';
import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import rehypeFormat from 'rehype-format';
import rehypeStringify from 'rehype-stringify';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';
import { cwd, pageDir, postDir } from './utils.mjs';
import rehypeHighlight from 'rehype-highlight';
import remarkGfm from 'remark-gfm';

const relPathMarkdownRegex = /(\/index)?\.mdx$/;

/**
 * Deprecated
 * @returns {Array.<{params: {id: string}}>} An array of objects with post ID params.
 */
export async function getAllPostIds() {
  const fileNames = await getAllPostFileNames();

  const allIds = fileNames?.map((fileName) => {
    const relativePath = fileName.replace(pageDir() + '/', '');
    const id = relativePath.replace(relPathMarkdownRegex, '');

    return {
      params: {
        id: id.split('/'),
      },
    };
  });

  return allIds;
}

/**
 * Single post data object
 * @typedef {Object} SinglePost
 * @property {string} SinglePost.id
 * @property {string} SinglePost.title
 * @property {string?} SinglePost.date
 * @property {string=} SinglePost.contentHtml
 */


function readPostFile(id) {
  let fullPath = path.join(postDir(), `${id}.md`);
  let fileContents = '';

  try {
    fileContents = fs.readFileSync(fullPath, 'utf8');
  } catch (err) {
    fullPath = path.join(postDir(), `${id}`, `index.mdx`);
    fileContents = fs.readFileSync(fullPath, 'utf8');
  }

  return { fullPath, fileContents };
}

/**
 * 
 * @param {Array.<string>} ids 
 * @returns {Promise.<PostData> | undefined}
 */
export async function getPostData(ids) {
  if (!ids || ids.length === 0) {
    return undefined;
  }

  const id = ids.join('/');

  const { fullPath, fileContents } = readPostFile(id);

  // Use gray-matter to parse the post metadata section
  const { data, content } = matter(fileContents);

  // Use remark to convert markdown into HTML string
  const processedContent = await unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(remarkRehype)
    .use(rehypeSlug)
    .use(rehypeAutolinkHeadings, {
      behavior: 'wrap',
    })
    .use(rehypeHighlight)
    .use(rehypeFormat)
    .use(rehypeStringify)
    .process(content);

  const contentHtml = processedContent.toString();

  // Combine the data with the id and contentHtml
  return {
    id,
    title: data.title ?? 'Untitled Post',
    contentHtml,
    date: data.date.toISOString() ?? null,
  };
}

export function getPostMeta(id) {
  const { fullPath, fileContents } = readPostFile(id);
  const { data, content } = matter(fileContents);

  return data;
}

/**
 * 
 * @param {string} src 
 * @param {function(Error, Array.<string>?)} callback 
 */
function getDirectories(
  src,
  callback
) {
  glob(src + '/**/*', callback);
}

/**
 * 
 * @returns {Promise<Array.<string>?>}
 */
async function getAllPostFileNames() {
  let allPaths = await new Promise((r) => {
    getDirectories(postDir(), (err, res) => {
      if (err) {
        console.log('Error', err);
        r(null);
      }
      r(res);
    });
  });

  const fileNames = allPaths?.filter((filename) => {
    if (!filename) {
      return false;
    }

    try {
      fs.readFileSync(filename, 'utf8');
    } catch (err) {
      return false;
    }

    const mdMatch = filename.match(/\.mdx$/);

    if (!mdMatch || mdMatch.length === 0) {
      return false;
    }

    return true;
  });

  return fileNames ?? null;
}

/**
 * 
 * @returns {Promise<number>} The number of posts found.
 */
export async function getPostCount() {
  const fileNames = await getAllPostFileNames();

  return fileNames?.length ?? 0;
}

/**
 * Return a list of post data for indexing. Content is raw Markdown.
 * @returns {Promise< Array.<{id: string, title: string, date: string?, content: string}>? >}
 */
export async function getAllPostDataIndex() {
  const fileNames = await getAllPostFileNames();

  if (!fileNames || fileNames.length === 0) {
    return null;
  }

  const postData = await Promise.all(
    fileNames.map(async (fileName) => {
      const fullPath = fileName;

      // Remove ".mdx" from file name to get id
      const relativePath = fileName.replace(pageDir(), '');
      const id = relativePath.replace(relPathMarkdownRegex, '');

      // Read markdown file as string
      const fileContents = fs.readFileSync(fileName, 'utf8');

      const { data, content } = matter(fileContents);

      // Combine the data with the id
      return {
        id,
        title: data.title ?? 'Untitled Post',
        date: data.date.toISOString() ?? null,
        content,
      };
    })
  );

  return postData;
}

/**
 * Post data object
 * @typedef {Object} PostData
 * @property {string} PostData.id
 * @property {string} PostData.title
 * @property {string?} PostData.date
 * @property {string} PostData.contentHtml
 * @property {string} PostData.relativePath
 * @property {string} PostData.fullPath
 * @property {boolean} PostData.isShort
 */

/**
 * 
 * @returns {Promise.<Array.<PostData>?>}
 */
export async function getAllPostData() {
  const fileNames = await getAllPostFileNames();

  if (!fileNames || fileNames.length === 0) {
    return null;
  }

  const allPostData = await Promise.all(
    fileNames.map(async (fileName) => {
      const fullPath = fileName;

      // Remove ".md" from file name to get id
      const relativePath = fileName.replace(pageDir(), '');
      const id = relativePath.replace(relPathMarkdownRegex, '');

      // Read markdown file as string
      const fileContents = fs.readFileSync(fullPath, 'utf8');

      const { data, content } = matter(fileContents);

      const isShort = content.length <= 500;

      const excerpt = isShort ? content : content.slice(0, 500).trim();

      // Use remark to convert markdown into HTML string
      const parseContent = await unified()
        .use(remarkParse)
        .use(remarkGfm)
        .use(remarkRehype)
        .use(rehypeSlug)
        .use(rehypeHighlight)
        .use(rehypeFormat)
        .use(rehypeStringify)
        .process(content);

      const contentHtml = parseContent.toString();

      const parseExcerpt = isShort
        ? parseContent
        : await unified()
          .use(remarkParse)
          .use(remarkGfm)
          .use(remarkRehype)
          .use(rehypeHighlight)
          .use(rehypeFormat)
          .use(rehypeStringify)
          .process(excerpt + ' ....');

      const excerptHtml = isShort ? contentHtml : parseExcerpt.toString();

      // Combine the data with the id
      return {
        id,
        relativePath,
        fullPath,
        title: data.title ?? 'Untitled Post',
        date: data.date?.toISOString() ?? null,
        contentHtml,
        excerptHtml,
        isShort,
      };
    })
  );

  return allPostData;
}

/**
 * 
 * @returns {Promise<Array.<PostData>?>}
 */
export async function getAllPostDataSorted() {
  const allPosts = await getAllPostData();

  if (allPosts === null) {
    return null;
  }

  // Sort posts by date
  return allPosts?.sort((a, b) => {
    if (a.date < b.date) {
      return 1;
    } else {
      return -1;
    }
  });
}

/**
 * 
 * @returns {Promise<Array.<PostData>?>}
 */
export async function getLatestPosts(props) {
  let allPosts = await getAllPostDataSorted();

  const latestPosts = allPosts?.slice(props.skip, props.skip + props.limit);

  return latestPosts;
}


/**
 * 
 * @param {{countPerPage: number}} props 
 * @returns {Promise.<number>=} The number of pages needed to show all posts when given a number of posts per page.
 */
export async function getPageCount(props) {
  if (!props || !props.countPerPage) {
    return undefined;
  }

  const postCount = await getPostCount();

  return Math.ceil(postCount / props.countPerPage);
}
